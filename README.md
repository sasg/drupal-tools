#Drupal-Tools
--------------
This is a command line utility designed to accomplish a series of tasks related to Drupal administration. Drupal-Tools uses the fabric and click modules to create an easy-to-use interface for gathering information across Drupal servers.

##Requirements:
1. Python 2.7
2. python-virtualenv, python-dev, python-pip (installed using apt-get)
3. fabric, prettytable, click python modules (installed via pip)
4. Access to master configuration file (/usr/sbin/master-pyconfig)

##Installation:
    sudo apt-get install python-virtualenv python-dev python-pip
    git clone https://aesaz@bitbucket.org/sasg/drupal-tools.git
    cd drupal-tools
    virtualenv venv
    venv/bin/pip install fabric prettytable click

##How to run:
Servers: server info (disk space, gateway, hostname, etc.) in table form, Stackcheck: returns a list of sites for each server

\*Note: "--output=show" is technically redundant as it is the default option
\*Note: You may need sudo permissions in order to write files depending on where the script is located

    cd drupal-tools
	venv/bin/python drupal-tools.py servers --output=show
    venv/bin/python drupal-tools.py servers --output=write
	venv/bin/python drupal-tools.py stackcheck --output=show
    venv/bin/python drupal-tools.py stackcheck --output=write

##In Progress:
- Run commands on specific IP with arguments
- How to regarding requirements.txt and dependency installation

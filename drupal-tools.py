#!/usr/bin/python
"""
drupal-tools

This is a command line utility designed to accomplish a series of tasks related to Drupal administration.
"""


import click
import sys
import os
import fabric
from fabric.api import *
sys.path.append("/usr/sbin/master-pyconfig/") # Allow imports from master-pyconfig
from config import hosts, user, password
from prettytable import PrettyTable


# Change encoding from ASCII to UTF-8
reload(sys)
sys.setdefaultencoding("utf8")


# Service account variables
env.user = user
env.password = password
env.hosts = hosts


# Global variables
server_info_list = []
boot_info_list = []
stack_check_dict = {}


#============= click commands


@click.group()
def cli():
    """Create the base CLI for running multiple commands"""
    pass


@cli.command()
@click.option("--output", default="show", help="\'show\' or \'write\' server info")
def servers(output):
    """Executes methods for grabbing server info"""
    execute(easyinfo)

    if output == "show":
        execute(show_servers) # Display tables in terminal 
    elif output == "write":
        execute(write_servers) # Write tables to text file

    # Disconnect from all servers
    fabric.network.disconnect_all()

    return None


@cli.command()
def modules():
    """Executes methods for generating module inventory"""
    execute(get_pmlist_text)

    # Create new instance of ModuleInventory
    inv = ModuleInventory()
    inv.get_dirs()
    inv.process_by_site()
    inv.write_by_site()
    inv.process_by_module()
    inv.write_by_module()

    # Disconnect from all servers
    fabric.network.disconnect_all()

    return None


@cli.command()
@click.option("--output", default="show", help="\'show\' or \'write\' server info")
def stackcheck(output):
    """Executes methods for checking site stacks on each server"""
    with hide("running", "stdout", "stderr", "status"):
        execute(site_stack_info)

        # Disconnect from all servers
        fabric.network.disconnect_all()

        if output == "show":
            stackcheck_print()
        elif output == "write":
            stackcheck_write()

    return None


#============= /click commands

#============= servers


def easyinfo():
    """Grabs information from servers and preps it for table building"""
    with hide("running", "stdout", "stderr", "status"):
        server_info = []
        server_info.append(get_hostname())
        diskinfo = get_diskinfo()
        boot_info_list.append(get_bootinfo())
        for item in diskinfo:
            server_info.append(item)

        server_info.append(get_kernel())
        server_info_list.append(server_info)
        netinfo = get_network_interfaces()
        
        for item in netinfo:
            server_info.append(item)  
    
    return None
        

@runs_once
def build_table():
    """Build table out of previously collected server info"""
    table_headers = [
        "Hostname", "Disk Size", "Used", "Avail", "Use%", "Kernel", "Address",
        "Netmask", "Gateway"
    ]
    # Create new instance of PrettyTable using above headers
    table = PrettyTable(table_headers)
    table.align["Hostname"] = "l"
    table.align["Disk Size"] = "r"
    table.align["Used"] = "r"
    table.align["Avail"] = "r"
    table.padding_width = 1
    
    for item in server_info_list:
        # Adds each element in server_info_list to row
        table.add_row(item)
        
    return table
    

@runs_once
def build_boot_table():
    """Build table out of previously collected server info"""
    table_headers = [
        "Hostname", "Size", "Used", "Avail", "Use%", "Partition"
    ]
    table = PrettyTable(table_headers)
    table.align["Hostname"] = "l"
    table.align["Size"] = "r"
    table.align["Used"] = "r"
    table.align["Avail"] = "r"
    table.padding_width = 1
    for item in boot_info_list:
        table.add_row(item)
    return table


@runs_once
def show_servers():
    """Shows server info table"""
    table = build_table()
    print("\n")
    print(table)
    print("\n")

    boottable = build_boot_table()
    print("\n")
    print(boottable)
    print("\n")
    
    return None
    
    
@runs_once
def write_servers():
    """Writes server info table to easyinfo_out.txt"""
    table = build_table()
    boottable = build_boot_table()

    with open("servers_out.txt", "w") as text:
        text.write(str(table))
        text.write("\n")
        text.write(str(boottable))
        text.write("\n")
    
    return None


def get_hostname():
    """Gets hostname"""
    return str(run("hostname"))
    

def get_diskinfo():
    """Gets disk space information"""
    df_output = run("df -h").split("\n")
    
    for i in range(len(df_output)):
        if "dm" in df_output[i] or "drupal" in df_output[i]:
            output_line = i       

    diskinfo = df_output[output_line].split(" ")[1:-1]
    # Remove any indices with empty strings 
    diskinfo = filter(None, diskinfo)
    
    return diskinfo

def get_bootinfo():
    """Gets boot partition information"""
    df_output = run("df -h").split("\n")

    for i in range(len(df_output)):
        if "boot" in df_output[i]:
            output_line = i
    
    bootinfo = df_output[output_line].split(" ")[1:-1]
    # Remove any indices with empty strings
    bootinfo = filter(None, bootinfo)
    bootinfo.insert(0, get_hostname())
    bootinfo.append("/boot")

    return bootinfo    


def get_kernel():
    """Gets kernel version"""
    return str(run("uname -r"))


def get_network_interfaces():
    """Gets necessary info from /etc/network/interfaces"""
    interfaces = run("cat /etc/network/interfaces").split("\n")
    address = None
    netmask = None
    gateway = None
    
    # Process for getting only what we need from interfaces
    for item in interfaces:
        net_key = item.split(" ")[0].strip()
        if net_key == "address":
            address = item.split(" ")[1].strip()
        elif net_key == "netmask":
            netmask = item.split(" ")[1].strip()
        elif net_key == "gateway":
            gateway = item.split(" ")[1].strip()
            
    return [address, netmask, gateway]


#============= /servers


#============= stackcheck

def site_stack_info():
    """Checks /var/www/web/sites for site directories and adds them to a dictionary"""
    # Grabs contents of /var/www/web/sites and splits them into a list 
    ls = run("ls /var/www/web/sites").split()
    sites = []

    # Traverse list to filter out directories or files that aren't sites
    for item in ls:
        if "edu" in item and "tar" not in item:
            # Sites get added to sites list
            sites.append(item)

    # Key = hostname, Value = list of sites
    stack_check_dict[get_hostname()] = sites
    
    return None


def stackcheck_print():
    """Prints formatted contents of stack_check_dict"""
    for item in sorted(stack_check_dict.keys()):
        print("\n" + str(item) + ": ")
        for value in stack_check_dict[item]:
            print("\t" + str(value))

    return None


def stackcheck_write():
    """Writes contents of stack_check_dict out to text file"""
    with open("stackcheck_out.txt", "w") as text:
        for item in sorted(stack_check_dict.keys()):
            text.write("\n" + str(item) + ": " + "\n")
            for value in stack_check_dict[item]:
                text.write("\t" + str(value) + "\n")
    
    return None


#============= /stackcheck


# Starts up command line interface when script is invoked
if __name__=="__main__":
	cli()
